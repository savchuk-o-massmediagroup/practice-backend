import {
    MinLength,
    Required,
    Property,
} from "@tsed/schema";

import {Column, Entity, ObjectIdColumn} from "typeorm";

@Entity()
export class User {
    @ObjectIdColumn()
    _id: string;

    @Required()
    @Column()
    @MinLength(3)
    login: string;

    @Required()
    @Column()
    @MinLength(6)
    password: string;

    @Column()
    token: string;

    verifyPassword(password: string) {
        return this.password === password;
    }
}
  