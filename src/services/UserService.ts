import { Injectable, Inject } from "@tsed/common";
import { UserRepository } from "../repositories/UserRepository";
import { User } from "../entity/User";
import { UseConnection } from "@tsed/typeorm";

@Injectable()
export class UsersService {

  @Inject()
  @UseConnection("mongoose")
  userRepository: UserRepository;

  async create(user: User): Promise<User> {
    await this.userRepository.save(user);
    console.log("Saved a new user with id: " + user._id);

    return user;
  }

  async findOne(user: object): Promise<User | undefined> {
    return await this.userRepository.findOne(user);
  }

  async findById(id: string): Promise<User | undefined>{
    return await this.userRepository.findOne(id);
  }

  async attachToken(user: User, token: string) {
    user.token = token;
    await this.userRepository.save(user);
  }

}