import { BodyParams, Controller, Get, HeaderParams, Post, Req } from "@tsed/common";
import { User } from "src/entity/User";
import { UsersService } from "src/services/UserService";
import { Authenticate, Authorize } from "@tsed/passport";
import { Required, Returns } from "@tsed/schema";

@Controller("/auth")
export class Auth {
    constructor(private usersService: UsersService) {
    }

    @Post("/reg")
    async post(@BodyParams() user: User): Promise<any> {
        await this.usersService.create(user);
    }

    @Post("/login")
    @Authenticate("local")
    login(@Req() req: Req, @Required() @BodyParams("login") login: string, @Required() @BodyParams("password") password: string) {
        return req.user;
    }

    @Get("/user-info")
    @Authorize("jwt")
    @Returns(200, User)
    info(@Req() req: Req, @HeaderParams("Authorization") token: string) {
        return req.user;
    }
}
