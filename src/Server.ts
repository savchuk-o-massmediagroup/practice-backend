import { Configuration, Inject } from "@tsed/di";
import { PlatformApplication } from "@tsed/common";
import "@tsed/platform-express"; // /!\ keep this import
import bodyParser from "body-parser";
import compress from "compression";
import cookieParser from "cookie-parser";
import methodOverride from "method-override";
import cors from "cors";
import "@tsed/ajv";
import { config, rootDir } from "./config";

@Configuration({
  mongoose: [
    {
      id: "default", // Recommended: define default connection. All models without dbName will be assigned to this connection
      url: "mongodb://127.0.0.1:27017/default",
      connectionOptions: {}
    },
  ],
  typeorm: [
    {
      name: "mongoose",
      type: "mongodb",
      entities: [
        `${__dirname}/entity/*{.ts,.js}`
      ],
      // migrations: [
      //   `${__dirname}/migrations/*{.ts,.js}`
      // ],
      // subscribers: [
      //   `${__dirname}/subscriber/*{.ts,.js}`
      // ]
    }
  ],
  ...config,
  acceptMimes: ["application/json"],
  httpPort: process.env.PORT || 8083,
  httpsPort: false, // CHANGE
  mount: {
    "/rest": [
      `${rootDir}/controllers/**/*.ts`
    ]
  },
  componentsScan: [
    `${rootDir}/services/**/**.ts`,
    `${rootDir}/repositories/**/**.ts`,
    `${rootDir}/middlewares/**/**.ts`,
    `${rootDir}/protocols/*.ts`
  ],
  passport: {},
  exclude: [
    "**/*.spec.ts"
  ]
})
export class Server {
  @Inject()
  app: PlatformApplication;

  @Configuration()
  settings: Configuration;

  $beforeRoutesInit(): void {
    this.app
      .use(cors())
      .use(cookieParser())
      .use(compress({}))
      .use(methodOverride())
      .use(bodyParser.json())
      .use(bodyParser.urlencoded({
        extended: true
      }));
  }
}
